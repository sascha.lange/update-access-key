package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/user"

	"github.com/aws/aws-sdk-go/service/iam"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"gopkg.in/ini.v1"
)

const constRegion = "eu-west-1"

type keyPair struct {
	id     string
	secret string
}

func main() {
	profileArg := flag.String("profile", "testuser", "AWS profile name for authentication")

	flag.Parse()

	// Define required parameters
	if *profileArg == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	// Set credentials file name with full path
	usr, err := user.Current()
	checkError("Cannot get user home directory", err)
	awsCredentialsFile := usr.HomeDir + "/.aws/credentials"
	awsCredentialsBackupFile := awsCredentialsFile + ".bak"

	// Create tmp and backup credentials file
	fmt.Println("Backing up credentials file")
	copyFile(awsCredentialsFile, awsCredentialsBackupFile)

	// Save old credentials in array
	oldKeyPair := getCurrentCredentials(awsCredentialsBackupFile, *profileArg)

	// Create AWS session
	sess := createSession(*profileArg)

	username := getUsername(sess)
	newKeyPair := createAccessKey(sess, username)

	setCredentials(awsCredentialsFile, *profileArg, newKeyPair.id, newKeyPair.secret)

	deleteKey(sess, oldKeyPair.id)
}

func deleteKey(sess *session.Session, keyID string) {
	fmt.Printf("Deleting old access key: %s\n", keyID)
	i := iam.New(sess)
	_, err := i.DeleteAccessKey(&iam.DeleteAccessKeyInput{
		AccessKeyId: aws.String(keyID),
	})
	checkError("Cannot delete access key\n", err)
	fmt.Println("Access key deleted")
}

func setCredentials(credentialsFile string, awsProfile string, k string, s string) {
	cfg, err := ini.Load(credentialsFile)
	checkError("Cannot read credentials file\n", err)

	// accessKey := cfg.Section("testuser").Key("aws_access_key_id").String()
	// secretKey := cfg.Section("testuser").Key("aws_secret_access_key").String()
	fmt.Println("Writting key to credentials file")
	cfg.Section("testuser").Key("aws_access_key_id").SetValue(k)
	cfg.Section("testuser").Key("aws_secret_access_key").SetValue(s)
	writeErr := cfg.SaveTo(credentialsFile)
	checkError("Cannot write credentials to file\n", writeErr)
	fmt.Println("Key written to credentials file")
}

func createAccessKey(sess *session.Session, user string) keyPair {
	fmt.Println("Creating new access key")
	i := iam.New(sess)
	newKey, err := i.CreateAccessKey(&iam.CreateAccessKeyInput{
		UserName: aws.String(user),
	})
	checkError("Cannot create new access key\n", err)
	k := keyPair{
		id:     *newKey.AccessKey.AccessKeyId,
		secret: *newKey.AccessKey.SecretAccessKey,
	}
	fmt.Printf("New access key: %s\n", k.id)

	return k
}

func getUsername(sess *session.Session) string {
	i := iam.New(sess)
	userInfo, err := i.GetUser(&iam.GetUserInput{})
	checkError("Cannot get current account informations\n", err)
	// id := *userInfo.User.UserId
	name := *userInfo.User.UserName
	return name
}

func createSession(profile string) *session.Session {
	var sess *session.Session
	if len(profile) <= 0 {
		sess = session.Must(session.NewSession(&aws.Config{
			Region: aws.String(constRegion),
		}))
		return sess
	}
	sess = session.Must(session.NewSessionWithOptions(session.Options{
		Config:  aws.Config{Region: aws.String(constRegion)},
		Profile: profile,
	}))
	return sess
}

func getCurrentCredentials(credentialsFile string, awsProfile string) keyPair {
	cfg, err := ini.Load(credentialsFile)
	checkError("Cannot read credentials file\n", err)

	// accessKey := cfg.Section("testuser").Key("aws_access_key_id").String()
	// secretKey := cfg.Section("testuser").Key("aws_secret_access_key").String()
	k := keyPair{
		id:     cfg.Section("testuser").Key("aws_access_key_id").String(),
		secret: cfg.Section("testuser").Key("aws_secret_access_key").String(),
	}

	return k
}

func copyFile(src, dst string) error {
	var err error
	var srcfd *os.File
	var dstfd *os.File
	var srcinfo os.FileInfo

	srcfd, err = os.Open(src)
	checkError("Cannot open source file\n", err)
	defer srcfd.Close()

	dstfd, err = os.Create(dst)
	checkError("Cannot create destination file\n", err)
	defer dstfd.Close()

	_, err = io.Copy(dstfd, srcfd)
	checkError("Cannot copy file\n", err)

	srcinfo, err = os.Stat(src)
	checkError("Cannot lookup source file\n", err)
	return os.Chmod(dst, srcinfo.Mode())
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}
